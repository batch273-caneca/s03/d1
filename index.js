// alert("Hello!");

//In JS, classes can be created yung the "class" keyword and {}
/*

//camelCase
//PascalCase

//Naming convention for classes: Begin with Uppercase character

	Syntax:
		class NameOfClass {	
		}

*/

class Student {
	//to enable students instantiated from this class to have distinct names and emails, 
	//our constructor must be able to accept name and email arguments 
	//which it will then use to set the value of the object's corresponding properties
	constructor(name, email, grades) {
		//this.key = value/parameter
		this.name = name;
		this.email = email;
		this.gradeAve = undefined;

		//Activity 1:
		if(grades.length === 4){
		     if(grades.every(grade => grade >= 0 && grade <= 100)){
		         this.grades = grades;
		     } else {
		         this.grades = undefined;
		     }
		 } else {
		     this.grades = undefined;
		 }
	}

	//Methods
	login() {
		console.log(`${this.email} has logged in.`);
		//console.log(this)
		return this;
	}

	logout() {
		console.log(`${this.email} has logged out.`);
		return this;
	}

	listGrades() {
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}.`);
		return this;
	}

	computeAve(){
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		//update property
		this.gradeAve = sum/4;
		//return object
		return this;
	}

	willPass() {
		return this.computeAve() >= 85 ? true : false;
	}

	willPassWithHonors() {
		if (this.willPass()) {
			if(this.computeAve() >= 90) {
				return true
			} else {
				return false;
			}

		} else {
			return undefined;
		}
	}

}

//To create an objects from classes, we use the keyword "new".
//The new keyword will look for a constructor method in the class being instantiated from in order to instantiate an object. 
//This method defines HOW objects will be instantiated from a class.
//Given that we have a Student class, creating instances of student objects from this class would be as follows:
// let studentOne = new Student('john', 'john@mail.com');
// let studentTwo = new Student();
// console.log(studentOne);

//Activity 1:
	 //FUNCTION CODING
//let studentOne = new Student('John', 'john@mail.com', [101, 84, 78, 88]);
//let studentOne = new Student('John', 'john@mail.com', [-10, 84, 78, 88]);
//let studentOne = new Student('John', 'john@mail.com', ['hello', 84, 78, 88]);
//let studentOne = new Student('John', 'john@mail.com', [84, 78, 88]);
let studentOne = new Student('John', 'john@mail.com', [89, 84, 78, 88]);
let studentTwo = new Student('Joe', 'joe@mail.com', [78, 82, 79, 85]);
let studentThree = new Student('Jane', 'jane@mail.com', [87, 89, 91, 93]);
let studentFour = new Student('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);
console.log(studentOne);
console.log(studentTwo);
console.log(studentThree);
console.log(studentFour);

//Getter and Setter
//Best practice dictates that we regulate access to such properties. 
//Getter - retrieval
//Setter - manipulation


/*
	Mini-Activity:
		Created a new class called Person

		This person class should be able to isntatiate a new object with the ff fields

		name,
		age (should be a number and must be greater than or equal to 18, otherwise set the property to undefined),
		nationality,
		address

		Instantiate 2 new objects from the Person class. person1 and person2

		Log both object in the console.

*/

class Person {
	constructor(name, age, nationality, address) {
		this.name = name;
		this.nationality = nationality;
		this.address = address;
		if (typeof age === "number" && age >=18) {
			this.age = age;
		} else {
			this.age = undefined;
		}
	}

}

let person1 = new Person("John Smith", 16, "American", "LA");
let person2 = new Person("Jane Doe", 20, "American", "San Francisco");
// console.log(person1);
// console.log(person2);

/*
Quiz 1:

1. What is the blueprint where objects are created from?
	Answer: Class

2. What is the naming convention applied to classes?
	Answer: upper-case first letter

3. What keyword do we use to create objects from a class?
	Answer: new

4. What is the technical term for creating an object from a class?
	Answer: instantiation

5. What class method dictates HOW objects will be created from that class?
	Answer: constructor
*/


/*
	QUIZ 2

1 Should class methods be included in the class constructor? Yes


2 Can class methods be separated by commas? No


3 Can we update an object's properties via dot notation? Yes


4 What do you call the methods used to regulate access to an object's properties? object.property object['property']


5 What does a method need to return in order for it to be chainable? Object



*/

// FUNCTION CODING

class Student {
	constructor(name, email, grades) {
		this.name = name;
		this.email = email;
		this.gradeAve = undefined;
		this.passed = undefined;
		this.passedWithHonors = undefined;

		if(grades.length === 4){
		     if(grades.every(grade => grade >= 0 && grade <= 100)){
		         this.grades = grades;
		     } else {
		         this.grades = undefined;
		     }
		 } else {
		     this.grades = undefined;
		 }
	}

	login() {
		console.log(`${this.email} has logged in`);
		return this;
	}
	logout() {
		console.log(`${this.email} has logged out`);
		return this
	}
	listGrades(){
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
		return this;
	}
	computeAve() {
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		this.gradeAve = sum/4;
		return this;
	}
	willPass() {
		this.passed = this.computeAve().gradeAve >= 85 ? true : false;
		return this;
	}

	willPassWithHonors() {
		if (this.passed) {
		    if (this.gradeAve >= 90) {
		        this.passedWithHonors = true;
		    } else {
		        this.passedWithHonors = false;
		    }
		} else {
		    this.passedWithHonors = false;
		}
		return this;
	}
}


let studentOne = new Student('John', 'john@mail.com', [89, 84, 78, 88]);
let studentTwo = new Student('Joe', 'joe@mail.com', [78, 82, 79, 85]);
let studentThree = new Student('Jane', 'jane@mail.com', [87, 89, 91, 93]);
let studentFour = new Student('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);

